var EngineEvent = {
	create: function(eventName) {
	
		return {
			eventName: eventName,
			callbacks: [],
			registerCallback: function(callback) {
				this.callbacks.push(callback);
			},
			unregisterCallback: function(callback) {
				var callbackIndex = this.callbacks.indexOf(callback);

				if (callbackIndex > -1) {
					this.callbacks.splice(callbackIndex, 1);
				}
			},
			fire: function(data) {
				var callbacks = this.callbacks.slice(0);
				callbacks.forEach(function(callback) { callback(data); });
			}
		};
	}, 
};

window.EngineTrigger = {
	events: {},

	dispatch: function(eventName, data) {
		var event = this.events[eventName];

		if (event) {
			event.fire(data);
		}
	},

	on: function(eventName, callback) {
		var event = this.events[eventName];

		if (!event) {
			event = EngineEvent.create(eventName);
			this.events[eventName] = event;
		}

		event.registerCallback(callback);
	},

	off: function(eventName, callback) {
		var event = this.events[eventName];

		if (event) {
			event.unregisterCallback(callback);

			if (event.callbacks.length === 0) {
				delete this.events[eventName];
			}
		}
	}
};