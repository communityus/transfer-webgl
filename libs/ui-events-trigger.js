window.FireUIEvent = function(eventName, payload) {
	var event = {
		eventName: eventName,
		payload: payload
	};

	var message = JSON.stringify(event);
	console.log('UI event fired: ', event);
	gameInstance.SendMessage('UIEventDispatcher', 'ProcessEvent', message);
};